#!/usr/bin/python3
from string import ascii_lowercase
from random import choices
from argparse import ArgumentParser
from smtplib import (
    SMTP,
    SMTPSenderRefused,
    SMTPRecipientsRefused,
)


def check_clear(domain, port, orgin_domain, from_addr, to_addr, log_level):
    with SMTP(host=domain, port=port, local_hostname=orgin_domain) as smtp:
        smtp.set_debuglevel(log_level)
        try:
            smtp.sendmail(
                from_addr=from_addr,
                to_addrs=[to_addr],
                msg="test_cleartextmail"
            )
        except SMTPSenderRefused as error:
            code = error.smtp_code
            error = error.smtp_error.decode()
            if code == 530 and "STARTTLS" in error:
                return False
            else:
                raise
        except SMTPRecipientsRefused:
            pass
        return True


def color(string, color_name):
    color_code = ""
    if color_name == "green":
        color_code = "\033[92m"
    elif color_name == "red":
        color_code = "\033[91m"
    elif color_name == "orange":
        color_code = "\033[93m"
    end_color = "\033[1m"
    return f"{color_code}{string}{end_color}"


def main():
    parser = ArgumentParser(
        description=(
            "Script to check wether or not the target mail server accepts "
            "cleartext connections and email from other smtp relay"
        )
    )
    parser.add_argument(
        'domain',
        type=str,
        help="domain to check"
    )

    parser.add_argument(
        '-p',
        '--port',
        type=int,
        default=25,
        help="port for STARTTLS (usually 25)",
    )
    parser.add_argument(
        '-f',
        '--from_addr',
        type=str,
        default="foo@example.com",
        help="address to use to check the server (default foo@example.com)"
    )
    parser.add_argument(
        '-t',
        '--to_addr',
        type=str,
        default="",
        help="address of the recipient (default [random]@[domain])"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Activate verbose mode an display all smtp trafic"
    )

    args = parser.parse_args()
    origin_domain = args.from_addr.split("@")[-1]
    rand_char = ''.join(choices(ascii_lowercase, k=64))
    to_addr = args.to_addr if args.to_addr else f"{rand_char}@{args.domain}"

    print(
        "Testing if the server support clear text email delivery:",
        end=' '
    )

    try:
        result = check_clear(
            domain=args.domain,
            port=args.port,
            orgin_domain=origin_domain,
            from_addr=args.from_addr,
            to_addr=to_addr,
            log_level=2 if args.verbose else 0
        )
    except Exception as exception:
        print(color("Unknown", "orange"))
        print(
            "got unknown exception run the command again with -v for more "
            "details"
        )
        print(exception)
    else:
        if result:
            print(color("True", "green"))
        else:
            print(color("False", "red"))


if __name__ == "__main__":
    main()
